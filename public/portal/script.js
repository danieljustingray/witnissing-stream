function redirectToIndex(username) {
    const commonPage = 'index_Common.htm';

    switch (username) {
        case 'Jason.Gray':
            window.location.href = 'coordinators_index.htm';
            break;
        default:
            window.location.href = 'index.htm'; // Default redirect
            break;
    }
}

function authenticate(event) {
    event.preventDefault();
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    const users = [
        { username: 'Daniel.Gray', password: 'DanielG1' },
        { username: 'Wim.DeJonge', password: 'ABC' },
        { username: 'Jason.Gray', password: 'ABC' },
        { username: 'Wilma.Gray', password: 'ABC' },
        { username: 'Kim.Posh', password: 'ABC' },
    ];

    const currentUser = users.find(user => user.username === username && user.password === password);

    if (currentUser) {
        redirectToIndex(username);
    } else {
        alert('Invalid credentials. Please try again.');
    }
}
